module Array2DTests exposing (rowLength)

import Array
import Array2D exposing (Array2D)
import Debug exposing (log)
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, intRange, list, string, tuple3)
import Random exposing (maxInt)
import Test exposing (Test, fuzz)
import Test.Runner


rowLength : Test
rowLength =
    fuzz
        (tuple3 ( intRange 1 50, intRange 1 50, int ))
        "Correct row length"
        (\( x, y, rowNum ) ->
            let
                array2d =
                    Array2D x y (Array.fromList (List.range 1 (x * y)))

                rowNumMod =
                    modBy y rowNum
            in
            Array2D.getRow rowNumMod array2d |> Array.length |> Expect.equal x
        )
