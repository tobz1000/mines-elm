module Array2D exposing (Array2D, get, getRow)

import Array exposing (Array)


type alias Array2D a =
    { xSize : Int
    , ySize : Int
    , contents : Array a
    }


get : Int -> Int -> Array2D a -> Maybe a
get x y { xSize, ySize, contents } =
    Array.get (y * xSize + x) contents


getRow : Int -> Array2D a -> Array a
getRow rowNum { xSize, ySize, contents } =
    Array.slice (rowNum * xSize) ((rowNum + 1) * xSize) contents
