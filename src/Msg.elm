module Msg exposing (Msg)


type Msg
    = CellClear Int Int
    | CellFlag Int Int
