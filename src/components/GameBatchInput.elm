module GameBatchInput exposing
    ( Model
    , Msg(..)
    , ParsedModel
    , emptyModel
    , render
    , toParsedModel
    , update
    )

import Css
import Html.Styled exposing (div, input, label, span, text)
import Html.Styled.Attributes exposing (css, placeholder, value)
import Html.Styled.Events exposing (onInput)


type Msg
    = CountPerSpecInput String
    | DimXRangeInput String
    | DimYRangeInput String
    | MinesRangeInput String
    | MetaseedInput String


type alias Model =
    { countPerSpec : String
    , dimXRange : String
    , dimYRange : String
    , minesRange : String
    , metaseed : String
    }


type alias ParsedModel =
    { countPerSpec : Int
    , dimXRange : List Int
    , dimYRange : List Int
    , minesRange : List Int
    , metaseed : Int
    }


emptyModel : Model
emptyModel =
    { countPerSpec = ""
    , dimXRange = ""
    , dimYRange = ""
    , minesRange = ""
    , metaseed = ""
    }


update : Msg -> Model -> Model
update msg model =
    case msg of
        CountPerSpecInput s ->
            { model | countPerSpec = s }

        DimXRangeInput s ->
            { model | dimXRange = s }

        DimYRangeInput s ->
            { model | dimYRange = s }

        MinesRangeInput s ->
            { model | minesRange = s }

        MetaseedInput s ->
            { model | metaseed = s }


render : Model -> Html.Styled.Html Msg
render { countPerSpec, dimXRange, dimYRange, minesRange, metaseed } =
    div [ css [ Css.displayFlex, Css.flexFlow1 Css.wrap ] ]
        [ renderInputField countPerSpecConfig countPerSpec
        , renderInputField dimYRangeConfig dimYRange
        , renderInputField dimXRangeConfig dimXRange
        , renderInputField minesRangeConfig minesRange
        , renderInputField metaseedConfig metaseed
        ]


renderInputField : InputConfig val -> String -> Html.Styled.Html Msg
renderInputField { labelText, default, parse, toMsg } content =
    let
        inputFieldErrorStyle =
            Css.backgroundColor (Css.hex "ffbbbb")

        inputFieldOkStyle =
            Css.backgroundColor (Css.hex "ffffff")

        validateStyle =
            case ( String.isEmpty content, parse content ) of
                ( False, Nothing ) ->
                    [ inputFieldErrorStyle ]

                _ ->
                    [ inputFieldOkStyle ]

        inputStyle =
            validateStyle
                ++ [ Css.width (Css.ch 7)
                   , Css.marginLeft (Css.ch 0.5)
                   , Css.marginRight (Css.ch 0.5)
                   ]

        labelStyle =
            [ Css.marginRight (Css.ch 0.5), Css.fontFamily Css.sansSerif ]
    in
    div [ css [ Css.whiteSpace Css.noWrap ] ]
        [ label [ css labelStyle ]
            [ text labelText
            , input
                [ css inputStyle, placeholder default, onInput toMsg, value content ]
                []
            ]
        ]


toParsedModel : Model -> Maybe ParsedModel
toParsedModel { countPerSpec, dimXRange, dimYRange, minesRange, metaseed } =
    case
        ( countPerSpecConfig.parse countPerSpec
        , dimXRangeConfig.parse dimXRange
        , ( dimYRangeConfig.parse dimYRange
          , minesRangeConfig.parse minesRange
          , metaseedConfig.parse metaseed
          )
        )
    of
        ( Just c, Just x, ( Just y, Just m, Just s ) ) ->
            Just
                { countPerSpec = c
                , dimXRange = x
                , dimYRange = y
                , minesRange = m
                , metaseed = s
                }

        _ ->
            Nothing


type alias InputConfig val =
    { labelText : String
    , default : String
    , parse : String -> Maybe val
    , toMsg : String -> Msg
    }


countPerSpecConfig : InputConfig Int
countPerSpecConfig =
    { labelText = "Count per spec"
    , default = "100"
    , parse = String.toInt
    , toMsg = CountPerSpecInput
    }


dimXRangeConfig : InputConfig (List Int)
dimXRangeConfig =
    { labelText = "# columns range"
    , default = "5-10"
    , parse = parseInputRange
    , toMsg = DimXRangeInput
    }


dimYRangeConfig : InputConfig (List Int)
dimYRangeConfig =
    { labelText = "# rows range"
    , default = "5-10"
    , parse = parseInputRange
    , toMsg = DimYRangeInput
    }


minesRangeConfig : InputConfig (List Int)
minesRangeConfig =
    { labelText = "# mines range"
    , default = "5-10"
    , parse = parseInputRange
    , toMsg = MinesRangeInput
    }


metaseedConfig : InputConfig Int
metaseedConfig =
    { labelText = "Metaseed"
    , default = "13337"
    , parse = String.toInt
    , toMsg = MetaseedInput
    }


parseInputRange : String -> Maybe (List Int)
parseInputRange input =
    case String.split "-" input of
        [ startStr, endStr ] ->
            case ( String.toInt startStr, String.toInt endStr ) of
                ( Just start, Just end ) ->
                    if start > 0 && start < end then
                        Just (List.range start end)

                    else
                        Nothing

                _ ->
                    Nothing

        _ ->
            Nothing
