module GameViewer exposing (render)

import Array exposing (Array)
import Array2D exposing (Array2D)
import Browser
import Cell
import Css
import Debug
import Html
import Html.Styled exposing (div, table, tbody, td, text, tr)
import Html.Styled.Attributes exposing (class, css)
import Http
import Json.Encode
import Server exposing (BatchResults, CellAction, GameBatch)
import Time


render : Array2D Cell.State -> Html.Styled.Html msg
render grid =
    div []
        [ table []
            [ tbody []
                gridXAxis grid.xSize :: (List.range 0 (grid.ySize - 1) |> List.map gridRow grid)
            ]
        ]


gridXAxis : Int -> Html.Styled.Html msg
gridXAxis size =
    let
        labelCell colNum =
            td [ css [ laminate, axisFont, axisXAlign ] ] [ text (String.fromInt colNum) ]
    in
    tr []
        (td [] List.range 0 size |> List.map labelCell)


gridRow : Array2D Cell.State -> Int -> Html.Styled.Html msg
gridRow states rowNum =
    let
        rowStates =
            Array2D.getRow rowNum states

        yAxisLabel =
            td [ css [ laminate, axisFont, axisYAlign ] ] [ text (String.fromInt rowNum) ]

        cells = List.map Cell.render (Array.toList rowStates)
    in
    tr [] yAxisLabel :: cells


laminate =
    Css.batch
        [ Css.cursor Css.default
        , Css.property "-moz-user-select" "none"
        , Css.property "-webkit-user-select" "none"
        , Css.property "-ms-user-select" "none"
        ]


axisFont =
    Css.batch [ Css.fontSize (Css.pct 70), Css.color (Css.hex "444444") ]


axisXAlign =
    Css.batch
        [ Css.textAlign Css.center
        , Css.verticalAlign Css.bottom
        , Css.paddingBottom Css.zero
        ]


axisYAlign =
    Css.batch
        [ Css.textAlign Css.right
        , Css.verticalAlign Css.center
        , Css.paddingRight Css.zero
        ]
