module Cell exposing (State(..), render)

import Browser
import Css
import Html.Styled exposing (Html, td, text)
import Html.Styled.Attributes exposing (css)


type State
    = Unknown
    | Mine
    | ToClear
    | Flagged
    | Cleared Int


render : State -> Html.Styled.Html msg
render state =
    td
        [ css
            [ Css.width (Css.px 20)
            , Css.height (Css.px 20)
            , Css.textAlign Css.center
            , Css.backgroundColor (cellColor state)
            , Css.fontFamily Css.sansSerif
            , Css.cursor Css.default
            ]
        ]
        [ text (cellText state) ]


cellText : State -> String
cellText state =
    case state of
        Cleared 0 ->
            ""

        Cleared surrCount ->
            String.fromInt surrCount

        _ ->
            ""


cellColor : State -> Css.Color
cellColor state =
    case state of
        Unknown ->
            Css.hex "bbbbbb"

        Mine ->
            Css.hex "ff5555"

        ToClear ->
            Css.hex "eeee66"

        Flagged ->
            Css.hex "9999ff"

        Cleared _ ->
            Css.hex "ffffff"
