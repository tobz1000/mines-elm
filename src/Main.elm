module Main exposing (main)

import Browser
import Server
import GameBatchInput
import Html.Styled exposing (text, div)
import Html.Styled.Attributes exposing (class, css)

type Msg
    = GameBatchInputMsg GameBatchInput.Msg
    | ClickRunBatch
    | RcvBatchResults { batch : Server.GameBatch, results : Server.BatchResults }

type alias Model = { input: GameBatchInput.Model, current: CurrentBatchModel }


type alias BatchResultsView =
    { batch : Server.GameBatch
    , results : Server.BatchResults
    , currentView : Maybe GameView
    }


type alias GameView =
    { info : Server.GameInfo, turn : Int }


type CurrentBatchModel
    = None
    | Waiting
    | Results BatchResultsView

main =
    Browser.element
        { init = init
        , view = view >> Html.Styled.toUnstyled
        , subscriptions = subscriptions
        , update = update
        }

init : () -> ( Model, Cmd Msg )
init () =
    ( { input = GameBatchInput.emptyModel
      , current = None
      }
    , Cmd.none )

view : Model -> Html.Styled.Html Msg
view { input, current } =
    div []
        [ GameBatchInput.render input |> Html.Styled.map GameBatchInputMsg
        -- render game batch output
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    -- Server.batchResults decodeBatchResults
    Sub.none

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GameBatchInputMsg i ->
            ({ model | input = GameBatchInput.update i model.input }, Cmd.none)

        ClickRunBatch -> (model, Cmd.none)
        RcvBatchResults _ -> (model, Cmd.none)
