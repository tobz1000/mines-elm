port module Server exposing (BatchResults, Cell, CellAction(..), GameBatch, GameInfo, GameState(..), SpecResult, TurnInfo, batchResults, gameBatch)

import Array2D exposing (Array2D)
import Json.Encode


type GameState
    = Ongoing
    | Win
    | Lose


type alias TurnInfo =
    { clearReq : List Int
    , clearActual : List Int
    , flagged : List Int
    , unflagged : List Int
    , cellsRem : Int
    , gameState : GameState
    }


type alias GameBatch =
    { countPerSpec : Int
    , dimsRange : ( List Int, List Int )
    , minesRange : List Int
    , autoclear : Bool
    , metaseed : Int
    }


type alias BatchResults =
    List SpecResult


type alias SpecResult =
    { dims : ( Int, Int )
    , mines : Int
    , played : Int
    , wins : Int
    , info : List GameInfo
    }


type alias GameInfo =
    { dims : ( Int, Int )
    , grid : Array2D Cell
    , mines : Int
    , seed : Int
    , autoclear : Bool
    , turns : List TurnInfo
    , cellsRem : Int
    , gameState : GameState
    }


type alias Cell =
    { mine : Bool
    , surrIndices : List Int
    , surrMineCount : Int
    }


type CellAction
    = NoAction
    | Flagged
    | Cleared


port gameBatch : Json.Encode.Value -> Cmd msg


port batchResults : (Json.Encode.Value -> msg) -> Sub msg
